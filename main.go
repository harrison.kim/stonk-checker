package main

import (
	"fmt"
	"os"

	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/alpacahq/alpaca-trade-api-go/common"
)

func init() {
	alpaca.SetBaseUrl(os.Getenv("ENDPOINT"))
}

func main() {
	alpacaClient := alpaca.NewClient(common.Credentials())
	acct, err := alpacaClient.GetAccount()
	if err != nil {
		panic(err)
	}

	fmt.Println(*acct)
	quoteResponse, err := alpacaClient.GetLastQuote("SPY")
	if err != nil {
		panic(err)
	}
	fmt.Println(quoteResponse.Symbol)
	fmt.Println(quoteResponse.Status)
	fmt.Println(quoteResponse.Last.AskPrice)
	fmt.Println(quoteResponse.Last.AskSize)
	fmt.Println(quoteResponse.Last.BidPrice)
	fmt.Println(quoteResponse.Last.BidExchange)
	fmt.Println(quoteResponse.Last.Timestamp)
}
